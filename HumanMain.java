package human;

import java.util.ArrayList;
import java.util.Scanner;

public class HumanMain {
    static ArrayList<Human> humanList = new ArrayList<Human>();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        start();
    }

    private static void start() {
        int choise;
        do {
            System.out.println("\n 1. Dodaj człowieka \n 2. Usuń człowieka \n 3. Sprawdz czy są równi" +
                    " \n 4. Wyswietl ludzi \n 0. Wyjście");
            choise = scanner.nextInt();
            yourChoice(scanner, choise);
        } while (choise != 0);
    }

    private static void yourChoice(Scanner scanner, int choise) {
        switch (choise) {
            case 1:
                addHuman();
                break;
            case 2:
                delateHuman(humanList);
                break;
            case 3:
                isEquals(humanList);
                break;
            case 4:
                printlist(humanList);
                break;
        }
    }

    private static void isEquals(ArrayList<Human> humanList) {
        if (humanList.size() < 2) {
            System.out.println("ERORR, za mało osób");
            return;
        }
        System.out.println("Wybierz 2 indesy osob które chcesz porównać: ");
        int person1 = scanner.nextInt();
        int person2 = scanner.nextInt();

        if (humanList.get(person1).equals(humanList.get(person2))) {
            System.out.println("Osoby są równe");
        } else {
            System.out.println("Osoby nie są równe");
        }
    }

    private static void delateHuman(ArrayList<Human> humanList) {
        if (humanList.size() == 0) {
            System.out.println("ERORR, Lista jest pusta");
            return;
        }
        printlist(humanList);
        System.out.println("Wybierz osobę którą chcesz usunać");
        int choice = scanner.nextInt();
        humanList.remove(choice);
    }

    private static void printlist(ArrayList<Human> humanList) {
        if (humanList.size() == 0) {
            System.out.println("ERORR, Lista jest pusta");
            return;
        }
        int indexOfHuman = 0;
        for (Human listOfHuman :
                humanList) {
            System.out.println(indexOfHuman + " " + listOfHuman.getAge() + " " +
                    listOfHuman.getLastname() + " " + listOfHuman.getName());
            indexOfHuman++;
        }
    }

    public static void addHuman() {
        System.out.println("Podaj wiek, imię i nazwisko człowieka");
        int age = scanner.nextInt();
        scanner.nextLine();
        String name = scanner.nextLine();
        String lastname = scanner.nextLine();
        Human human = new Human(age, name, lastname);
        humanList.add(human);
    }
}



