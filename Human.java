package human;

import java.util.Objects;

public class Human {
    private int age;
    private String name;
    private String lastname;

    public Human(int age, String name, String lastname){
        this.age=age;
        this.name=name;
        this.lastname=lastname;
    }

    public int getAge() {
        return age;
    }

    public String getLastname() {
        return lastname;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                Objects.equals(name, human.name) &&
                Objects.equals(lastname, human.lastname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, name, lastname);
    }
}
